package xyz.sontieu.listviewinlistview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.sontieu.listviewinlistview.R;
import xyz.sontieu.listviewinlistview.model.ChildModel;

/**
 * Project : ListView in ListView
 * User    : iantsu
 * Date    : 11/07/2016
 */
public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ChildViewHolder> {
    private Context context;
    private List<ChildModel> list;
    private ParentAdapter.ChildItemClickListener listener;

    public ChildAdapter(Context context, List<ChildModel> list, ParentAdapter.ChildItemClickListener listener) {
        this.context = context;
        this.list= list;
        this.listener = listener;
    }


    @Override
    public ChildViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.child_item, parent, false);

        return new ChildViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChildViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getName());

        switch (list.get(position).getStatus()) {
            case 0:
                // do nothing
                break;
            case 1:
                holder.cooking.setVisibility(View.GONE);
                break;
            case 2:
                holder.repay.setVisibility(View.GONE);
                break;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onChildItemClick(list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name) TextView name;
        @BindView(R.id.cooking) TextView cooking;
        @BindView(R.id.repay) TextView repay;

        public ChildViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
