package xyz.sontieu.listviewinlistview.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.sontieu.listviewinlistview.R;
import xyz.sontieu.listviewinlistview.model.ChildModel;
import xyz.sontieu.listviewinlistview.model.ParentModel;

/**
 * Project : ListView in ListView
 * User    : iantsu
 * Date    : 11/07/2016
 */
public class ParentAdapter extends RecyclerView.Adapter<ParentAdapter.ParentViewHolder> {

    private Context context;
    private List<ParentModel> list;
    private ParentItemClickListener parentListener;
    private ChildItemClickListener childListener;

    public ParentAdapter(Context context, List<ParentModel> list) {
        this.context = context;
        this.list = list;
    }

    public interface ParentItemClickListener {
        void onParentItemClick(ParentModel item);
    }

    public void setParentItemClickListener(ParentItemClickListener listener) {
        this.parentListener = listener;
    }

    public interface ChildItemClickListener {
        void onChildItemClick(ChildModel item);
    }

    public void setChildItemClickListener(ChildItemClickListener listener) {
        this.childListener = listener;
    }

    @Override
    public ParentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.parent_item, parent, false);

        return new ParentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ParentViewHolder holder, final int position) {
        holder.tvStatus.setText(list.get(position).getStatus());
        holder.tvStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Status child view click " + position, Toast.LENGTH_SHORT).show();
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        llm.setAutoMeasureEnabled(true);
        holder.childList.setLayoutManager(llm);

        ChildAdapter childAdapter = new ChildAdapter(context, list.get(position).getChilds(), childListener);
        holder.childList.setAdapter(childAdapter);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentListener != null)
                    parentListener.onParentItemClick(list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ParentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvStatus) TextView tvStatus;
        @BindView(R.id.childList) RecyclerView childList;

        public ParentViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
