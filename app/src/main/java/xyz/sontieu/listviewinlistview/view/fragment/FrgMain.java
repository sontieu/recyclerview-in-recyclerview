package xyz.sontieu.listviewinlistview.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.sontieu.listviewinlistview.R;
import xyz.sontieu.listviewinlistview.adapter.ParentAdapter;
import xyz.sontieu.listviewinlistview.model.ChildModel;
import xyz.sontieu.listviewinlistview.model.ParentModel;

/**
 * Created by Mobitouch on 7/12/2016.
 */
public class FrgMain extends Fragment
        implements ParentAdapter.ParentItemClickListener, ParentAdapter.ChildItemClickListener{
    private Context context;

    @BindView(R.id.list) RecyclerView recyclerView;

    public static FrgMain newInstance() {
        FrgMain frg = new FrgMain();

        return frg;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, true);

        ButterKnife.bind(this, v);
        getData();
        setupUi();

        return v;
    }

    private void setupUi() {
        LinearLayoutManager llm = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        llm.setAutoMeasureEnabled(true);
        recyclerView.setLayoutManager(llm);
    }

    private void getData() {
        List<ParentModel> list = Arrays.asList(
                new ParentModel("Status 1", Arrays.asList(new ChildModel("Parent 1 Item 1", 0)
                        , new ChildModel("Parent 1 Item 2", 0)
                )),
                new ParentModel("Status 2", Arrays.asList(new ChildModel("Parent 2 Item 1", 0),
                        new ChildModel("Parent 2 Item 2", 1)
                ))
        );

        ParentAdapter adapter = new ParentAdapter(context ,list);
        adapter.setParentItemClickListener(this);
        adapter.setChildItemClickListener(this);

        if (recyclerView != null) {
            if (adapter != null)
                recyclerView.setAdapter(adapter);
            else
                Log.d("SONTIEU", "Adapter null");

        } else {
            Log.d("SONTIEU", "RecyclerView NUll");
        }
    }

    @Override
    public void onChildItemClick(ChildModel item) {
        Toast.makeText(context, "Fragment Child click : " + item.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onParentItemClick(ParentModel item) {
        Toast.makeText(context, "Fragment Parent click : " + item.getStatus(), Toast.LENGTH_SHORT).show();
    }
}
