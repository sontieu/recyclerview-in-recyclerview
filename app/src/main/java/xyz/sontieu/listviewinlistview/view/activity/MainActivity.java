package xyz.sontieu.listviewinlistview.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.sontieu.listviewinlistview.R;
import xyz.sontieu.listviewinlistview.adapter.ParentAdapter;
import xyz.sontieu.listviewinlistview.model.ChildModel;
import xyz.sontieu.listviewinlistview.model.ParentModel;
import xyz.sontieu.listviewinlistview.view.fragment.FrgMain;

public class MainActivity extends AppCompatActivity
    implements ParentAdapter.ParentItemClickListener, ParentAdapter.ChildItemClickListener{

//    @BindView(R.id.mainRecyclerView) RecyclerView recyclerView;
    @BindView(R.id.btnFragment) Button btnFrg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        getData();
        setupUi();
    }

    private void setupUi() {
        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        llm.setAutoMeasureEnabled(true);
//        recyclerView.setLayoutManager(llm);
    }

    private void getData() {
        List<ParentModel> list = Arrays.asList(
                new ParentModel("Status 1", Arrays.asList(new ChildModel("Parent 1 Item 1", 0)
                                                            , new ChildModel("Parent 1 Item 2", 0)
                )),
                new ParentModel("Status 2", Arrays.asList(new ChildModel("Parent 2 Item 1", 0),
                                        new ChildModel("Parent 2 Item 2", 1)
                ))
        );

        ParentAdapter adapter = new ParentAdapter(MainActivity.this ,list);
        adapter.setParentItemClickListener(this);
        adapter.setChildItemClickListener(this);

//        if (recyclerView != null) {
//            if (adapter != null)
//                recyclerView.setAdapter(adapter);
//            else
//                Log.d("SONTIEU", "Adapter null");
//
//        } else {
//            Log.d("SONTIEU", "RecyclerView NUll");
//        }
    }

    @Override
    public void onChildItemClick(ChildModel item) {
        Toast.makeText(MainActivity.this, "Child click : " + item.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onParentItemClick(ParentModel item) {
        Toast.makeText(MainActivity.this, "Parent click : " + item.getStatus(), Toast.LENGTH_SHORT).show();
    }

//    @OnClick(R.id.btnFragment)
//    public void changeToFragment() {
//        Log.d("SONTIEU", "changeToFragment");
//        //recyclerView.setVisibility(View.GONE);
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.mainPage, FrgMain.newInstance(), "FRAGMENT_MAIN")
//                .commit();
//    }
}
