package xyz.sontieu.listviewinlistview.model;

import java.util.List;

/**
 * Project : ListView in ListView
 * User    : iantsu
 * Date    : 11/07/2016
 */
public class ParentModel {
    private String status;
    private List<ChildModel> childs;

    public ParentModel() {

    }

    public ParentModel(String status, List<ChildModel> childs) {
        this.status = status;
        this.childs = childs;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ChildModel> getChilds() {
        return childs;
    }

    public void setChilds(List<ChildModel> childs) {
        this.childs = childs;
    }
}
