package xyz.sontieu.listviewinlistview.model;

/**
 * Project : ListView in ListView
 * User    : iantsu
 * Date    : 11/07/2016
 */
public class ChildModel {
    private String name;
    private int status;

    public ChildModel() {

    }

    public ChildModel(String name, int status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
